<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="container">
        <div class="row">
            <div class=" col-xs-12">
                <div class="blog-hero-image">
                    <?php
                        if( !empty( $hero_image = get_field( 'hero_image' ) ) ) {
                            $skip_lazy = true; // skip lazy loading for first image to improve paint times
                            echo fx_get_image_tag( $hero_image, 'img-responsive', '', $skip_lazy, [ 'alt' => get_the_title() ]);
                            $skip_lazy = false;
                        }
                    ?>
                </div>
                <?php the_content(); ?>
                <?php get_template_part( 'partials/social-share' ); ?>
            </div>
        </div>
    </div>

<?php endwhile; endif; ?>

<?php get_footer();