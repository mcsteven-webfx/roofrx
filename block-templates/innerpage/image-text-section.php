<?php
    $top_spacing_desktop = get_field('top_spacing_desktop');
    $bottom_spacing_desktop = get_field('bottom_spacing_desktop');

    $top_spacing_tablet = get_field('top_spacing_tablet');
    $bottom_spacing_tablet = get_field('bottom_spacing_tablet');

    $top_spacing_mobile = get_field('top_spacing_mobile');
    $bottom_spacing_mobile = get_field('bottom_spacing_mobile');

    $slanted_option = get_field('slanted_option');

    if ($slanted_option == 'both') {
        $slanted_corner = 'skew';
    } elseif ($slanted_option == 'top') {
        $slanted_corner = 'slanted-top-corner skew';
    } else {
        $slanted_corner = '';
    }
?>

<section class="image-text-section <?php echo $slanted_corner; ?> desktop-top<?php echo $top_spacing_desktop; ?> tablet-top<?php echo $top_spacing_tablet; ?> mobile-top<?php echo $top_spacing_mobile; ?> desktop-bottom<?php echo $bottom_spacing_desktop; ?> tablet-bottom<?php echo $bottom_spacing_tablet; ?> mobile-bottom<?php echo $bottom_spacing_mobile; ?> about">
    <div class="about-background hidden-lg">
         <?php
            $skip_lazy = true; // skip lazy loading for first image to improve paint times
            echo fx_get_image_tag( 521, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Mobile background image' ] );
            $skip_lazy = false;
        ?>
    </div>
    <div class="about-content">
        <div class="container">
            <div class="about-wrapper">
                <?php if (get_field('image_position') != 'right') : ?>
                    <div class="about-image">
                        <?php
                            $skip_lazy = true; // skip lazy loading for first image to improve paint times
                            $image_id = get_field('image');
                            if ( $image_id ) {
                                echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Image text section' ] );
                            }
                            $skip_lazy = false;
                        ?>
                    </div>
                <?php endif; ?>
                <?php
                    $background_color = get_field('background_color');
                    $content_color_class = '';
                    if ($background_color == '#FFF' || $background_color == '#DCE0E1' || $background_color == '#e1efde') {
                        $content_color_class = 'black-color-content';
                    }
                ?>
                <div class="about-text <?php echo $content_color_class; ?>" style="background-color: <?php echo get_field('background_color'); ?>;">
                    <?php the_field('content'); ?>
                    <?php if( !empty( $link = get_field( 'button_link' ) ) ) : ?>
                        <a class="btn btn-primary"
                            href="<?php echo esc_url( $link['url'] ); ?>"
                            title="<?php echo esc_attr( $link['title'] ); ?>"
                            <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                            <?php the_field('button_label'); ?>
                        </a>
                    <?php endif; ?>
                </div>
                <?php if (get_field('image_position') != 'left') : ?>
                    <div class="about-image">
                        <?php
                            $skip_lazy = true; // skip lazy loading for first image to improve paint times
                            $image_id = get_field('image');
                            if ( $image_id ) {
                                echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Image text section' ] );
                            }
                            $skip_lazy = false;
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>