<?php
    $posts_per_page         = get_field('post_per_page');
    $post_order_by          = get_field('order_by');
    $post_order             = get_field('order');
    $featured_post          = get_field('display_featured_testimonial');
    $testimonial_category   = get_field('testimonial_category');
    $background_color       = get_field('background_color');
    $meta_key               = '';
    $section_background_color = '';

    if ($featured_post) {
        $meta_key = 'featured';
    }

    if ($background_color) {
        $section_background_color = 'style="background-color: '.$background_color.'"';
    }

    $args = array(
        'post_type'      => 'testimonial',
        'post_status'    => 'publish',
        'posts_per_page' => $posts_per_page,
        'orderby'        => esc_attr($post_order_by),
        'order'          => esc_attr($post_order),
        'meta_key'       => $meta_key,
        'meta_value'     => '1',
        'tax_query'      =>  $testimonial_category ? array(array('taxonomy'=>'testimonial_category','field'=>'category','terms'=>$testimonial_category)) : ''
    );

    $loop = new WP_Query( $args );

    $posts_result_count = $loop->found_posts;

    $post_shown_count = $loop->post_count;

    if ( $loop->have_posts() ) :
?>

<!-- Testimonial Section -->
    <section class="testimonial-section" <?php echo $section_background_color; ?>>
        <?php if( !empty( $background_image = get_field( 'background_image' ) ) ) : ?>
            <div class="testimonial-background-image">
                <?php
                    $skip_lazy = true; // skip lazy loading for first image to improve paint times
                    echo fx_get_image_tag( $background_image, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Testimonial background image' ] );
                    $skip_lazy = false;
                ?>
            </div>
        <?php endif; ?>
        <div class="container">
            <?php if ($testimonial_heading = get_field('heading')) : ?>
                <h2><?php echo $testimonial_heading; ?></h2>
            <?php endif; ?>
            <div class="testimonial-listings">
                <ul>
                    <?php while ( $loop->have_posts() ) : $loop->the_post();
                        $post_id = get_the_ID();
                        $client_name = get_field('client_name' , $post_id );
                        $platform_logo = get_field('platform_logo' , $post_id );
                        $testimonial_content = get_field('testimonial_content', $post_id);
                        $rating = get_field('testimonial_rating', $post_id);
                    ?>
                       <li>
                            <div class="testimonial-columns">
                                <span class="testimonial-date"><?php the_date(); ?></span>
                                <?php echo $testimonial_content; ?>
                                <div class="testimonials-author-info">
                                    <div class="platform-logo">
                                        <?php
                                            $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                            if ( $platform_logo ) {
                                                echo fx_get_image_tag( $platform_logo, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Platform Logo' ] );
                                            }
                                            $skip_lazy = false;
                                        ?>
                                    </div>
                                    <div class="author-info">
                                        <h5><?php echo $client_name; ?></h5>
                                        <?php if ( !empty($rating) ) { ?>
                                            <ul>
                                                <?php  for ($i = 0; $i < $rating; $i++) { ?>
                                                    <li><i class="icon-star"></i></li>
                                                <?php }  ?>
                                            </ul>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php
                        endwhile;
                        wp_reset_postdata();
                    ?>
                </ul>

                <div class="loading-area">
                    <?php
                        echo '<p>Showing <span class="post-shown">'. $post_shown_count .'</span> of <span class="total-post-count">'. $posts_result_count .'</span> reviews</p>';
                        echo '<div class="loading-bar"><div class="loading-bar-active"></div></div>';
                        echo '<a href="#" class="btn btn-primary js-testimonial-loadmore">Load more reviews</a>';
                    ?>
                </div>
            </div>
        </div>
        <div class="icon-shape">
            <?php
                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                echo fx_get_image_tag( 258, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Icon Shape' ] );
                $skip_lazy = false;
            ?>
        </div>
    </section>
<!-- Testimonial Section -->
<?php endif; ?>
