<?php $slanted_background = get_field('slanted_background'); ?>
<!-- image-background-section -->
<section class="image-background-section <?php if( $slanted_background == 'yes' ) echo 'skew'; ?>">
    <?php if( $slanted_background == 'yes' ) { echo '<div class="skew-wrapper">'; } ?>
        <div class="image-background-image">
            <?php
                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                $image_id = get_field('image_background');
                if ( $image_id ) {
                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Full width background image' ] );
                }
                $skip_lazy = false;
            ?>
        </div>
        <div class="image-background-content">
            <div class="container">
                <div class="image-background-content-wrapper">
                    <h3><?php the_field('heading'); ?></h3>
                    <?php the_field('content'); ?>
                    <?php if( !empty( $link = get_field( 'button_link' ) ) ) : ?>
                        <a class="btn btn-primary"
                            href="<?php echo esc_url( $link['url'] ); ?>"
                            title="<?php echo esc_attr( $link['title'] ); ?>"
                            <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                            <?php the_field('button_label'); ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php if( $slanted_background == 'yes' ) { echo '</div>'; } ?>
</section>
<!-- image-background-section -->