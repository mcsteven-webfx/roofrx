<section class="half-text-contact">
	<div class="container">
		<div class="row content-contact-flex-wrapper">
			<div class="col-md-7 content-left">
				<?php the_field('text_half'); ?>
			</div>
			<div class="col-md-5">
				<div class="contact-form-wrapper">
					<?php the_field('contact_half'); ?>
				</div>
			</div>
		</div>
	</div>
</section>