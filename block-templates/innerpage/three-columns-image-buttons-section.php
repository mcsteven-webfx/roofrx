<?php if( have_rows( 'image_buttons' ) ): ?>
<section class="image-buttons">
    <div class="image-buttons-wrapper">
        <div class="container">
            <h4><?php the_field('heading'); ?></h4>
            <div class="image-buttons-list">
                <div class="row">
                    <?php
                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                        while( have_rows( 'image_buttons' ) ): the_row();
                            if( !empty( $link = get_sub_field( 'link' ) ) ) :
                    ?>
                                <div class="col-sm-4">
                                    <a href="<?php echo esc_url( $link['url'] ); ?>"
                                        title="<?php echo esc_attr( $link['title'] ); ?>"
                                        <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                                        <div class="buttons-column services-2">
                                            <div class="buttons-image">
                                                <?php
                                                    $image_id = get_sub_field('image');
                                                    if ( $image_id ) {
                                                        echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => $link['title'] ] );
                                                    }
                                                ?>
                                            </div>
                                            <div class="buttons-info">
                                                <div class="buttons-info-text">
                                                    <h4><?php echo $link['title']; ?></h4>
                                                    <span class="btn btn-secondary">Learn more about  <?php echo $link['title']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                    <?php
                            endif;
                        endwhile;
                        $skip_lazy = false;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="buttons-icon-shape">
        <?php
            $skip_lazy = true; // skip lazy loading for first image to improve paint times
            echo fx_get_image_tag( 870, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Buttons icon shape' ] );
            $skip_lazy = false;
        ?>
    </div>
</section>

<?php endif; ?>
