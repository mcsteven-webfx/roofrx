<?php
    if( have_rows( 'accordion' ) ) :
        $i = 0;
        $background_color = get_field('background_color');

        if ($background_color != 'Textured') :
?>
            <section class="accordion-section">
                <div class="container">
                    <div class="accordion-wrapper">
                        <div class="accordion">
                            <?php
                                while( have_rows( 'accordion' ) ): the_row();
                                $i++;
                            ?>
                                <h3 class="<?php echo (($i % 2)==0) ? 'even' : 'odd'?>"><?php the_sub_field('heading'); ?></h3>
                                <div class="accordion-content">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php if ($background_color == 'Textured') : ?>
            <section class="accordion-section black-accordion-section">
                <div class="container">
                    <div class="accordion-wrapper">
                        <div class="accordion-wrapper-bg">
                            <?php
                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                echo fx_get_image_tag( 832, 'img-responsive', $skip_lazy );
                                $skip_lazy = false;
                            ?>
                        </div>
                        <div class="accordion">
                            <?php
                                while( have_rows( 'accordion' ) ): the_row();
                                $i++;
                            ?>
                                <h3 class="<?php echo (($i % 2)==0) ? 'even' : 'odd'?>"><?php the_sub_field('heading'); ?></h3>
                                <div class="accordion-content">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
<?php endif; ?>