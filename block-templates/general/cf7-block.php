<section class="page-contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <?php if ( get_field('content') ) : ?>
                    <div class="contact-top-content">
                        <?php the_field('content'); ?>
                    </div>
                <?php endif; ?>

                <div class="contact-form">
                    <?php
                        $form = get_field('cf7_shortcode');
                        if( !empty( $form ) ) {
                            echo apply_shortcodes( $form );
                        }
                    ?>
                   <?php if ( get_field('short_note') ) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-col">
                                   <p><?php the_field('short_note'); ?></p>
                                </div>
                            </div>
                        </div>
                   <?php endif; ?>
                </div>
            </div>

            <div class="col-lg-3 col-lg-offset-1">
                <div class="contact-sidebar">
                     <?php
                        // gets contact information set in Theme Settings
                        $email      = fx_get_client_email( true );
                        $phone      = fx_get_client_phone_number();
                        $phone_link = fx_get_client_phone_number( true );
                    ?>
                    <div class="contact-columns">
                        <h4>Email Address</h4>
                        <?php if( !empty( $email ) ) : ?>
                            <p><a href="<?php echo esc_url( sprintf( 'mailto:%s', $email ) ); ?>"> <?php echo $email; ?></a></p>
                        <?php endif; ?>
                    </div>
                    <div class="contact-columns">
                        <h4>Our locations</h4>
                        <div class="sub-col">
                             <?php if( have_rows( 'location_information', 'options' ) ) : ?>
                                 <?php while( have_rows( 'location_information', 'options' ) ) : the_row();
                                    $phone_number = get_sub_field( 'phone_number' );
                                ?>
                                    <p><span><?php the_sub_field( 'heading' ); ?>:</span>
                                        <a href="<?php echo esc_url( sprintf( 'tel:%s', $phone_number ) ); ?>">
                                            <?php echo $phone_number; ?>
                                        </a>
                                    </p>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="contact-columns">
                        <h4>Hours</h4>
                        <div class="sub-col">
                            <?php if( have_rows( 'store_hour', 'options' ) ) : ?>
                                 <?php while( have_rows( 'store_hour', 'options' ) ) : the_row();
                                    $day = get_sub_field( 'day' );
                                ?>
                                    <p><span> <?php the_sub_field( 'day' ); ?>:</span> <?php the_sub_field( 'time' ); ?></p>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="icon-shape-contact hidden-md-down">
             <?php
                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                echo fx_get_image_tag( 870, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Icon shape background' ] );
                $skip_lazy = false;
            ?>
        </div>
    </div>
</section>