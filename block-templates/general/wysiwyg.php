<?php
	$background_color = get_field('background_color');

    if ($background_color) {
        $section_background_color = 'style="background-color: '.$background_color.';--bg-color: '.$background_color.'"';
    }

    $top_spacing_desktop = get_field('top_spacing_desktop');
    $bottom_spacing_desktop = get_field('bottom_spacing_desktop');

    $top_spacing_tablet = get_field('top_spacing_tablet');
    $bottom_spacing_tablet = get_field('bottom_spacing_tablet');

    $top_spacing_mobile = get_field('top_spacing_mobile');
    $bottom_spacing_mobile = get_field('bottom_spacing_mobile');

    $slanted_option = get_field('slanted_option');

    if ($slanted_option == 'both') {
    	$slanted_corner = 'skew';
    } elseif ($slanted_option == 'top') {
    	$slanted_corner = 'slanted-top-corner skew';
    } else {
    	$slanted_corner = '';
    }

?>

<?php if ($background_color != 'Textured') : ?>
	<!-- white-wysiwyg -->
	<section class="white-wysiwyg <?php echo $slanted_corner; ?> desktop-top<?php echo $top_spacing_desktop; ?> tablet-top<?php echo $top_spacing_tablet; ?> mobile-top<?php echo $top_spacing_mobile; ?> desktop-bottom<?php echo $bottom_spacing_desktop; ?> tablet-bottom<?php echo $bottom_spacing_tablet; ?> mobile-bottom<?php echo $bottom_spacing_mobile; ?>" <?php echo $section_background_color; ?>>
	    <div class="container">
	        <div class="wysiwyg-content">
				<?php the_field( 'content' ); ?>
			</div>
		</div>
	</section>
	<!-- white-wysiwyg -->
<?php endif; ?>

<?php if ($background_color == 'Textured') : ?>
	<!-- black-wysiwyg -->
	<section class="black-wysiwyg <?php echo $slanted_corner; ?> desktop-top<?php echo $top_spacing_desktop; ?> tablet-top<?php echo $top_spacing_tablet; ?> mobile-top<?php echo $top_spacing_mobile; ?> desktop-bottom<?php echo $bottom_spacing_desktop; ?> tablet-bottom<?php echo $bottom_spacing_tablet; ?> mobile-bottom<?php echo $bottom_spacing_mobile; ?>">
	    <div class="black-wysiwyg-background">
	         <?php
	            $skip_lazy = true; // skip lazy loading for first image to improve paint times
	            echo fx_get_image_tag( 832, 'img-responsive', '', $skip_lazy, [ 'alt' => 'wysiwyg black background' ] );
	            $skip_lazy = false;
	        ?>
	    </div>
	    <div class="black-wysiwyg-wrapper">
	        <div class="black-wysiwyg-content">
	            <div class="container">
	                <div class="wysiwyg-content">
	                	<?php the_field( 'content' ); ?>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<!-- black-wysiwyg -->
<?php endif; ?>