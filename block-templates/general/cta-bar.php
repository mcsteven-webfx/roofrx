<?php if( !empty( $link = get_field( 'button_link' ) ) ) : ?>

    <section class="cta">
        <div class="container">
            <div class="row">
                <div class="cta-wrapper">
                    <h3>Contact us today to receive your free estimate! </h3>
                    <a class="btn btn-primary"
                        href="<?php echo esc_url( $link['url'] ); ?>"
                        title="<?php echo esc_attr( $link['title'] ) ;?>"
                        <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                        <?php the_field( 'button_label' ); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>