<?php
    $args = array(
        'post_type' => 'testimonial',
        'post_status' => 'publish',
        'meta_key' => 'featured',
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_value' => '1',
    );
    $loop = new WP_Query( $args );

    if ( $loop->have_posts() ) :
?>

    <!-- testimonials -->
    <section class="testimonials">
        <div class="container">
            <div class="testimonials-wrapper">
                <div class="testimonials-image hidden-md-down">
                    <?php
                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                        $image_id = get_field('featured_image');
                        if ( $image_id ) {
                            echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Testimonial Featured Image' ] );
                        }
                        $skip_lazy = false;
                    ?>
                    <div class="testimonial-quotes"><i class="icon-quote"></i></div>
                </div>
                <div class="testimonials-content">
                    <h3><?php the_field('heading'); ?></h3>
                    <div class="testimonials-slider">
                        <?php while ( $loop->have_posts() ) : $loop->the_post();
                            $post_id = get_the_ID();
                            $client_name = get_field('client_name' , $post_id );
                            $platform_logo = get_field('platform_logo' , $post_id );
                            $testimonial_content = get_field('testimonial_content', $post_id);
                        ?>
                            <div class="testimonials-item">
                                <div class="testimonials-columns">
                                    <?php echo $testimonial_content; ?>
                                    <div class="testimonials-info">
                                        <div class="google-logos">
                                            <?php
                                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                                if ( $platform_logo ) {
                                                    echo fx_get_image_tag( $platform_logo, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Platform Logo' ] );
                                                }
                                                $skip_lazy = false;
                                            ?>
                                        </div>
                                        <div class="info-cols">
                                            <h5><?php echo $client_name; ?></h5>
                                            <ul>
                                                <li><i class="icon-star"></i></li>
                                                <li><i class="icon-star"></i></li>
                                                <li><i class="icon-star"></i></li>
                                                <li><i class="icon-star"></i></li>
                                                <li><i class="icon-star"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                    </div>
                    <?php if( !empty( $link = get_field( 'button_link' ) ) ) : ?>
                        <div class="testimonials-btn">
                            <a class="btn btn-primary"
                                href="<?php echo esc_url( $link['url'] ); ?>"
                                title="<?php echo esc_attr( $link['title'] ); ?>"
                                <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                                <?php the_field('button_label'); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonials -->

<?php endif; ?>
