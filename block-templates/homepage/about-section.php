<section class="about skew">
    <div class="skew-wrapper">
        <div class="about-background">
            <?php
                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                echo fx_get_image_tag( 521, 'img-responsive', '', $skip_lazy, [ 'alt' => 'About background pattern' ] );
                $skip_lazy = false;
            ?>
        </div>
        <div class="about-content">
            <div class="container">
                <div class="about-wrapper">
                    <?php
                        $image_position = get_field('image_position');
                        if ($image_position != 'right') :
                    ?>
                        <div class="about-image">
                            <?php
                                $image_id = get_field('image');
                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                if ( $image_id ) {
                                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'About section image' ] );
                                }
                                $skip_lazy = false;
                            ?>
                        </div>
                    <?php endif; ?>
                    <div class="about-text">
                        <h2><?php the_field('heading'); ?></h2>
                        <?php the_field('content'); ?>
                        <?php if( !empty( $link = get_field( 'button_link' ) ) ) : ?>
                            <a class="btn btn-primary"
                                href="<?php echo esc_url( $link['url'] ); ?>"
                                title="<?php echo esc_attr( $link['title'] ); ?>"
                                <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                                <?php the_field('button_label'); ?>
                            </a>
                        <?php endif; ?>
                    </div>

                    <?php
                        $image_position = get_field('image_position');
                        if ($image_position != 'left') :
                    ?>
                        <div class="about-image">
                            <?php
                                $image_id = get_field('image');
                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                if ( $image_id ) {
                                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'About section image' ] );
                                }
                                $skip_lazy = false;
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>