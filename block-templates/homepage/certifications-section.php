<?php if( have_rows( 'certifications_logo' ) ): ?>

    <section class="certifications">
        <div class="container">
            <div class="certifications-wrapper">
                <h4><?php the_field('heading'); ?></h4>
                <div class="certifications-area">
                    <div class="certifications-slider">
                        <?php
                            $skip_lazy = true; // skip lazy loading for first image to improve paint times
                            while( have_rows( 'certifications_logo' ) ): the_row();
                        ?>
                        <div class="certifications-item">
                            <div class="certifications-logo">
                                <?php
                                    $image_id = get_sub_field('logo');
                                    $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                    if ( $image_id ) {
                                        echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Certification logo' ] );
                                    }
                                    $skip_lazy = false;
                                ?>
                            </div>
                        </div>
                        <?php
                            endwhile;
                            $skip_lazy = false;
                        ?>
                    </div>
                    <div class="progress hidden-lg" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <span class="slider__label sr-only"></span>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>
