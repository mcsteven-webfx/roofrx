<?php
    $args = array(
        'post_type' => 'service-areas',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $loop = new WP_Query( $args );
    $tab_id = 1;

   $heading = get_field('heading');
   $content = get_field('content');
?>

<?php if ( $loop->have_posts() ) : ?>
    <section class="service-area skew">
        <div class="service-area-skew-wrapper">
            <div class="container">
                <div class="service-area-top-content hidden-lg">
                    <h2><?php echo $heading; ?></h2>
                    <?php echo $content; ?>
                </div>
                <div class="tabs">
                    <div class="service-area-wrapper">
                        <div class="service-area-details">
                            <div class="tabs-content">
                                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <div class="tab <?php echo $tab_id == 1 ? 'active' : ''; ?>" tab-id="<?php echo $tab_id; ?>">
                                        <div class="service-area-items">
                                            <div class="service-area-column">
                                                <div class="service-area-mage">
                                                    <?php
                                                        $google_map = get_field('google_map_image', get_the_ID());
                                                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                                        if ( $google_map ) {
                                                            echo fx_get_image_tag( $google_map, 'img-responsive', 'location-image', $skip_lazy, [ 'alt' => get_the_title() ]);
                                                        }
                                                        $skip_lazy = false;
                                                    ?>
                                                </div>
                                                <div class="service-area-info">
                                                    <?php the_excerpt(); ?>
                                                    <a href="<?php the_permalink(); ?>" class="btn btn-secondary" target="_blank">
                                                        <?php the_title(); ?> location details <span></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                    $tab_id++;
                                    endwhile;
                                    wp_reset_postdata();
                                ?>
                                <div class="tabs-nav">
                                    <i class="material-icons icon-chevron-left prev-arrow" id="prev" ripple="ripple" ripple-color="#FFF"></i>
                                    <i class="material-icons icon-chevron-right next-arrow" id="next" ripple="ripple" ripple-color="#FFF"></i>
                                </div>
                            </div>
                        </div>

                        <div class="service-area-list">
                            <div class="service-area-top-content hidden-md-down">
                                <h2><?php echo $heading; ?></h2>
                                <?php echo $content; ?>
                            </div>
                            <div class="tabs-header">
                                <div class="row">
                                    <?php
                                        $list_tab_id = 1;
                                        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                            <div class="<?php echo $list_tab_id == 1 ? 'active' : ''; ?> tab-list-content col-xxs-6 col-sm-12 col-lg-3">
                                                <a href="" tab-id="<?php echo $list_tab_id; ?>" ripple="ripple" ripple-color="#FFF">
                                                    <div class="area-list-column">
                                                        <div class="area-list-image hidden-md-down">
                                                            <?php
                                                                $featured_image = get_field('featured_image', get_the_ID());
                                                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                                                if ( $featured_image ) {
                                                                    echo fx_get_image_tag( $featured_image, 'img-responsive', '', $skip_lazy, [ 'alt' => get_the_title() ] );
                                                                }
                                                                $skip_lazy = false;
                                                            ?>
                                                        </div>
                                                        <h4><?php the_title(); ?></h4>
                                                    </div>
                                                </a>
                                            </div>
                                    <?php
                                        $list_tab_id++;
                                        endwhile;
                                        wp_reset_postdata();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>