<section class="project-gallery">
    <div class="project-shape">
        <?php
            $skip_lazy = true; // skip lazy loading for first image to improve paint times
            echo fx_get_image_tag( 646, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Project Shape' ] );
            $skip_lazy = false;
        ?>
    </div>
    <div class="container">
        <div class="project-gallery-top-content">
            <?php
                $heading        = get_field('heading');
                $button_label   = get_field('button_label');
                $button_link    = get_field('button_link');
            ?>
            <?php if ( $heading ) { ?>
                <h2><?php the_field('heading'); ?></h2>
            <?php  } ?>
            <?php if ( $button_label ) { ?>
                <a href="<?php echo $button_link ? $button_link : '#' ;  ?>" class="btn btn-secondary">
                    <?php echo $button_label; ?>
                </a>
            <?php } ?>
        </div>

        <?php
            $args = array(
                'post_type' => 'project-gallery',
                'post_status' => 'publish',
                'posts_per_page' => 8,
                'orderby' => 'date',
                'order' => 'ASC',
            );
            $loop = new WP_Query( $args );
            $count = 0;
        ?>

        <?php if ( $loop->have_posts() ) : ?>
            <div class="project-gallery-slider-wrapper js-project-gallery">
                <div class="project-gallery-slider js-project-gallery-slider">
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <div class="project-gallery-item">
                            <a class="gallery-image external" href="<?php echo get_the_post_thumbnail_url(); ?>" target="_blank" rel="noopener" data-fancybox="gallery-images">
                                <div class="project-gallery-image">
                                    <?php
                                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                        if ( has_post_thumbnail() ) {
                                            echo fx_get_image_tag( get_post_thumbnail_id(), 'img-responsive', '', $skip_lazy, [ 'alt' => 'Project Image' ] );
                                        }
                                        $skip_lazy = false;
                                    ?>
                                </div>
                                <div class="project-gallery-info">
                                    <?php $post_terms = get_the_terms( get_the_ID(), 'project-gallery_category' ); ?>
                                    <?php if ($post_terms) : ?>
                                        <h4><?php echo $post_terms[0]->name; ?></h4>
                                    <?php endif; ?>
                                    <p><?php echo get_the_date( 'F Y' ); ?></p>
                                </div>
                            </a>
                        </div>
                    <?php
                        endwhile;
                        wp_reset_postdata();
                    ?>
                </div>
                <div class="progress js-project-gallery-ui-slider" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <span class="slider__label sr-only"></span>
                </div>
                <!-- <button class="js-project-gallery-before">&larr;</button>
                <button class="js-project-gallery-after">&rarr;</button> -->
            </div>
        <?php endif; ?>
    </div>
</section>