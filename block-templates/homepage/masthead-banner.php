<!-- masthead banner -->
<section class="masthead">
    <?php $skip_lazy = true; // skip lazy loading for first image to improve paint times ?>
        <div class="masthead-background-image">
            <?php
                $image_id = get_field('banner');
                if ( $image_id ) {
                    echo fx_get_image_tag( $image_id, 'masthead-slide__img', '', $skip_lazy, [ 'alt' => 'Hero Banner' ] );
                }
            ?>
        </div>
        <div class="masthead-background-content">
            <div class="container">
                <div class="masthead-background-content-wrapper">
                    <p><?php the_field( 'sub_heading' ); ?></p>
                    <h1><?php the_field( 'heading' ); ?></h1>
                    <?php if( !empty( $link = get_field( 'button_link' ) ) ) : ?>
                        <a class="btn btn-primary"
                            href="<?php echo esc_url( $link['url'] ); ?>"
                            title="<?php echo esc_attr( $link['title'] ) ;?>"
                            <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                            <?php the_field( 'button_label' ); ?>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="masthead-testimonial hidden-xs-down">
                    <?php the_field('content'); ?>
                    <div class="testimonial-info">
                        <div class="info-col">
                            <h5><?php the_field('client_name'); ?></h5>
                            <ul>
                                <li><i class="icon-star"></i></li>
                                <li><i class="icon-star"></i></li>
                                <li><i class="icon-star"></i></li>
                                <li><i class="icon-star"></i></li>
                                <li><i class="icon-star"></i></li>
                            </ul>
                        </div>
                        <div class="platform-logo">
                            <?php
                                $image_id = get_field('platform_logo');
                                if ( $image_id ) {
                                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Platform Logo' ] );
                                }
                            ?>
                        </div>
                    </div>
                    <div class="testimonial-quote"><i class="icon-quote"></i></div>
                </div>
            </div>
        </div>
    <?php $skip_lazy = false; ?>
</section>
<!-- masthead banner -->
