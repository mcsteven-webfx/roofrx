<section class="roofing-services">
    <div class="container">
        <div class="services-wrapper">
            <div class="services-left">
                <div class="services-content">
                    <h2><?php the_field( 'heading_left_content' ); ?></h2>
                    <p><?php the_field( 'content_left_content' ); ?>
                        <?php if( !empty( $link = get_field( 'link_left_content' ) ) ) : ?>
                            <span>
                                <a class="btn btn-secondary"
                                    href="<?php echo esc_url( $link['url'] ); ?>"
                                    title="<?php echo esc_attr( $link['title'] ); ?>"
                                    <?php if( $link['target'] ) printf( 'target="%s"', $link['target']); ?>>
                                    View all
                                </a>
                            </span>
                        <?php endif; ?>
                    </p>
                </div>
                <?php
                    $image_left_content = get_field('image_left_content');
                    if( $image_left_content ) :
                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                ?>
                    <div class="row hidden-md-down">
                        <div class="col-sm-12">
                            <?php if( !empty( $link_image_left_content = $image_left_content['link'] ) ) : ?>
                                <a href="<?php echo esc_url( $link_image_left_content['url'] ); ?>"
                                    title="<?php echo esc_attr( $link_image_left_content['title'] ); ?>"
                                    <?php if( $link_image_left_content['target'] ) printf( 'target="%s"', $link_image_left_content['target']); ?>>
                                    <div class="services-column services-1">
                                        <div class="services-image">
                                            <?php
                                                $image_id = $image_left_content['image'];
                                                if ( $image_id ) {
                                                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => $image_left_content['heading'] ] );
                                                }
                                            ?>
                                        </div>
                                        <div class="services-info">
                                            <span><?php echo $image_left_content['heading']; ?></span>
                                            <p class="hidden-md-down"><?php echo $image_left_content['content']; ?></p>
                                        </div>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php
                        $skip_lazy = false;
                    endif;
                ?>
            </div>
            <div class="services-right">
                <div class="services-list">
                    <div class="row">
                        <?php
                            if( $image_left_content ) :
                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                        ?>
                            <div class="col-sm-6 hidden-lg">
                                <a href="<?php echo esc_url( $link_image_left_content['url'] ); ?>"
                                    title="<?php echo esc_attr( $link_image_left_content['title'] ); ?>"
                                    <?php if( $link_image_left_content['target'] ) printf( 'target="%s"', $link_image_left_content['target']); ?>>
                                    <div class="services-column services-1">
                                        <div class="services-image">
                                            <?php
                                                $image_id = $image_left_content['image'];
                                                if ( $image_id ) {
                                                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => $image_left_content['heading'] ] );
                                                }
                                            ?>
                                        </div>
                                        <div class="services-info">
                                           <span><?php echo $image_left_content['heading']; ?></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php
                                $skip_lazy = false;
                            endif;
                        ?>

                        <?php
                            $large_section = get_field('large_section');
                            if( $large_section ) :
                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                        ?>
                            <div class="col-sm-6 col-lg-12">
                                <?php if( !empty( $link_large_section = $large_section['link'] ) ) : ?>
                                    <a href="<?php echo esc_url( $link_large_section['url'] ); ?>"
                                        title="<?php echo esc_attr( $link_large_section['title'] ); ?>"
                                        <?php if( $link_large_section['target'] ) printf( 'target="%s"', $link_large_section['target']); ?>>
                                        <div class="services-column services-1 services-roofs">
                                            <div class="services-image">
                                                <?php
                                                    $image_id = $large_section['image'];
                                                    if ( $image_id ) {
                                                        echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => $large_section['heading'] ] );
                                                    }
                                                ?>
                                            </div>
                                            <div class="services-info">
                                                <span><?php echo $large_section['heading']; ?></span>
                                                <p class="hidden-md-down"><?php echo $large_section['content']; ?></p>
                                            </div>
                                        </div>
                                    </a>
                                <?php endif; ?>
                            </div>
                        <?php
                                $skip_lazy = false;
                            endif;
                        ?>

                        <?php
                            if( have_rows( 'small_section' ) ) :
                                while( have_rows( 'small_section' ) ) : the_row();
                                    $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                    if( have_rows( 'small_image_buttons' ) ) :
                                        while( have_rows( 'small_image_buttons' ) ) : the_row();
                                            $link_small_section = get_sub_field('link');
                        ?>
                                            <div class="col-sm-4">
                                                <a href="<?php echo esc_url( $link_small_section['url'] ); ?>"
                                                    title="<?php echo esc_attr( $link_small_section['title'] ); ?>"
                                                    <?php if( $link_small_section['target'] ) printf( 'target="%s"', $link_small_section['target']); ?>>
                                                    <div class="services-column services-2">
                                                        <div class="services-image">
                                                            <?php
                                                                $image_id = get_sub_field('image');
                                                                if ( $image_id ) {
                                                                    echo fx_get_image_tag( $image_id, 'img-responsive', '', $skip_lazy, [ 'alt' => $link_small_section['title'] ] );
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="services-info">
                                                            <div class="services-info-text">
                                                                <?php echo $link_small_section['title']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                        <?php
                                        endwhile;
                                    endif;
                                    $skip_lazy = false;
                                endwhile;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>