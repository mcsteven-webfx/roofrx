<?php

/**
 * $template note:
 *
 * Block names should be prefixed with acf/. So if the name you specified in
 * fx_register_block is 'your-block-name', the name you should use here is
 * 'acf/your-block-name'
 */

$template = [
	['acf/homepage-masthead-banner'],
    ['acf/homepage-roofing-services'],
    ['acf/homepage-about-section'],
    ['acf/homepage-service-area-section'],
	['acf/wysiwyg'],
    ['acf/homepage-project-gallery-section'],
    ['acf/homepage-testimonial-section'],
    ['acf/homepage-certifications-section'],
    ['acf/general-cta-bar'],
];

?>

<div class="home-blocks-section">
    <InnerBlocks template="<?php echo esc_attr( wp_json_encode( $template ) )?>" templateLock="all" />
</div>
