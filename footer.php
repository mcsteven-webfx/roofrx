        <?php
            // gets contact information set in Theme Settings
            $address    = fx_get_client_address();
            $email      = fx_get_client_email( true );
            $phone      = fx_get_client_phone_number();
            $phone_link = fx_get_client_phone_number( true );
            $facebok_link = fx_get_social_facebook();
            $yelp_link = fx_get_social_yelp();
            $instagram_link = fx_get_social_instagram();
            $home_url   = get_home_url();
        ?>
        <footer class="page-footer">
            <div class="footer-texture-bg">
                <?php
                    $skip_lazy = true; // skip lazy loading for first image to improve paint times
                    echo fx_get_image_tag( 149, 'img-responsive', $skip_lazy );
                    $skip_lazy = false;
                ?>
            </div>
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="footer-top">
                            <div class="footer-left">
                                <div class="footer-logo-column">
                                    <div class="footer-logo">
                                        <a href="<?php echo esc_url( $home_url ); ?>">
                                            <?php
                                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                                echo fx_get_image_tag( 150, 'img-responsive', $skip_lazy );
                                                $skip_lazy = false;
                                            ?>
                                        </a>
                                    </div>
                                    <div class="footer-social-email">
                                        <?php if( !empty( $email ) ): ?>
                                            <p>Email Us: <a href="<?php echo esc_url( sprintf( 'mailto:%s', $email ) ); ?>"> <?php echo $email; ?></a></p>
                                        <?php endif; ?>
                                        <ul class="social-media hidden-lg">
                                            <li><a href="<?php echo $yelp_link; ?>"> <i class="icon-yelp"></i></a></li>
                                            <li><a href="<?php echo $facebok_link; ?>"><i class="icon-facebook"></i></a></li>
                                            <li><a href="<?php echo $instagram_link; ?>"><i class="icon-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php if( have_rows( 'location_information', 'options' ) ): ?>
                                    <div class="footer-address-column">
                                        <?php while( have_rows( 'location_information', 'options' ) ): the_row();
                                            $phone_number = get_sub_field( 'phone_number' );
                                        ?>
                                            <div class="address-column">
                                                <?php if ( get_sub_field( 'heading' ) ) { ?>
                                                    <h4><?php the_sub_field( 'heading' ); ?> Location</h4>
                                                <?php } ?>
                                                <p><i class="icon-pin"></i><?php the_sub_field( 'address' ); ?></p>
                                                <p><i class="icon-phone"></i>
                                                    <a href="<?php echo esc_url( sprintf( 'tel:%s', $phone_number ) ); ?>">
                                                        <?php echo $phone_number; ?>
                                                    </a>
                                                </p>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="footer-right hidden-md-down">
                                <?php wp_nav_menu(
                                        array(
                                            'theme_location' => 'footer_menu',
                                            'menu_class'     => 'menu',
                                            'container_class'=> 'quick-link'
                                        )
                                    );
                                ?>
                                <ul class="social-media">
                                    <li><a href="<?php echo $yelp_link; ?>"> <i class="icon-yelp"></i></a></li>
                                    <li><a href="<?php echo $facebok_link; ?>"><i class="icon-facebook"></i></a></li>
                                    <li><a href="<?php echo $instagram_link; ?>"><i class="icon-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="back-top hidden-lg">
                                <a href="javascript:void(0);" id="back-to-top" class="back-to-top"> BACK TO TOP  <i class="icon-arrow-up"></i> </a>
                            </div>
                            <div class="copyright">Copyright © 2021. All Rights Reserved.</div>
                            <div class="footer-secondary-menu clearfix">
                                <ul class="clearfix">
                                    <li><a href="<?php echo esc_url( $home_url ); ?>/site-credits">Site Credits</a></li>
                                    <li><a href="<?php echo esc_url( $home_url ); ?>/sitemap">Sitemap</a></li>
                                    <li><a href="<?php echo esc_url( $home_url ); ?>/privacy-policy">Privacy Policy </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <?php if( is_front_page() ): ?>
            <div class="page-popup">
                <div class="popup-container">
                    <div class="popup-wrapper">
                        <span class="icon-close"></span>
                        <h2>Before you leave, Request a Free Quote!</h2>
                        <div class="popup-contact">
                            <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1" html_id="cf7-form-4"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php wp_footer(); ?>
    </body>
</html>
