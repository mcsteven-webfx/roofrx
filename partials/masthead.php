<!-- masthead-inner -->
<section class="masthead-inner">
    <div class="masthead-inner-image">
        <?php
            $skip_lazy = true; // skip lazy loading for first image to improve paint times
            echo fx_get_image_tag( 1701, 'img-responsive', '', $skip_lazy, [ 'alt' => 'Inner Banner' ] );
            $skip_lazy = false;
        ?>
    </div>
    <div class="masthead-inner-content">
        <div class="container">
            <?php if ( is_search() ): ?>
                <h1>Search Results</h1>
             <?php elseif ( is_post_type_archive( 'service-areas' ) ): ?>
                <h1>Service Area</h1>
            <?php elseif ( is_home() || is_archive() ): ?>
                <h1>Blog</h1>
            <?php elseif ( is_404() ) : ?>
                <h1>404: Oops! We can't find the page you're looking for.</h1>
            <?php else : ?>
                <h1><?php the_title(); ?></h1>
            <?php endif; ?>
            <!-- breadcrumbs -->
            <?php
                if( function_exists( 'yoast_breadcrumb' ) ) {
                    yoast_breadcrumb( '<div class="breadcrumbs"><ul class="clearfix">', '</ul></div>' );
                }
            ?>
            <!-- breadcrumbs -->
        </div>
    </div>
</section>
<!-- masthead-inner -->