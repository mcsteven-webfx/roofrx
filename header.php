<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />

    <link href="https://fonts.googleapis.com/css2?family=Urbanist:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- font-family: 'Urbanist', sans-serif; -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php // Insert Google Fonts <link> here. Please use &display=swap in your URL! 
    ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <?php
        // gets client logo image set in Theme Settings
        $logo_id    = fx_get_client_logo_image_id(); 
        $home_url   = get_home_url();
    ?>

    <header class="page-header" id="page-header">
        <div class="top-link hidden-xs-down hidden-lg">
            <ul class="top-links clearfix">
                <li><a href="tel:(239) 789-9218"><i class="icon-phone"></i> Cape Coral: <span> (239) 789-9218</span></a></li>
                <li><a href="tel:(941) 979-5250"><i class="icon-phone"></i> PUNTA GORDA: <span>(941) 979-5250</span></a></li>
            </ul>
        </div>
        <div class="container">
            <div class="row">
                <div class="header-main clearfix">
                    <div class="logo">
                        <a href="<?php echo esc_url( $home_url ); ?>">
                            <?php
                                $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                echo fx_get_image_tag( $logo_id, 'img-responsive', '', $skip_lazy, [ 'alt' => 'logo' ] );
                                $skip_lazy = false;
                            ?>
                        </a>
                    </div>
                    <div class="header-right">
                        <div class="header-info clearfix">
                            <ul class="top-links hidden-md-down clearfix">
                                <li><a href="tel:(239) 789-9218"><i class="icon-phone hidden-lg"></i> Cape Coral: <span><i class="icon-phone hidden-md-down"></i> (239) 789-9218</span></a></li>
                                <li><a href="tel:(941) 979-5250"><i class="icon-phone  hidden-lg"></i> PUNTA GORDA: <span><i class="icon-phone hidden-md-down"></i> (941) 979-5250</span></a></li>
                            </ul>
                            <div class="toggle-menu hidden-xs-down hidden-lg"><span class="icon-menu-bar"></span> menu</div>
                            <div class="fixed-quote-btn hidden-xs-down hidden-lg"><a href="<?php echo esc_url( $home_url ); ?>/contact-us" class="btn btn-primary">Contact us</a></div>
                            <div class="search-content hidden-xs-down">
                                <form role="search" action="<?php echo esc_url( $home_url ); ?>" method="get">
                                    <input type="text" name="s" id="s" value="" placeholder="Search our site...">
                                    <button type="submit"><i class="icon-search"></i></button>
                                </form>
                            </div>
                            <div class="search-content hidden-sm-up">
                                <form role="search" action="<?php echo esc_url( $home_url ); ?>" method="get">
                                    <input type="text" name="s" id="s" value="" placeholder="Search...">
                                    <button type="submit"><i class="icon-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="header-fixed-bar">
                        <div class="fixed-logo">
                            <a href="<?php echo esc_url( $home_url ); ?>">
                                <?php
                                    $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                    echo fx_get_image_tag( 152, 'img-responsive', '', $skip_lazy, [ 'alt' => 'logo' ] );
                                    $skip_lazy = false;
                                ?>
                            </a>
                        </div>
                        <div class="search-content">
                            <form role="search" action="<?php echo esc_url( $home_url ); ?>" method="get">
                                <input type="text" name="s" id="s" value="" placeholder="Search our site...">
                                <button type="submit"><i class="icon-search"></i></button>
                            </form>
                        </div>
                        <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'main_menu',
                                    'menu_class'     => 'menu',
                                    'container_class'=> 'nav-primary clearfix'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- nav-fixed -->
        <div class="nav-fixed hidden-sm-up clearfix">
            <div class="fixed-quote-btn"><a href="<?php echo esc_url( $home_url ); ?>/contact-us" class="btn btn-primary">Contact us</a></div>
            <div class="call-btn"><a href="tel:(239) 789-9218"><i class="icon-phone"></i> Call now</a></div>
            <div class="toggle-menu"><span class="icon-menu-bar"></span> menu</div>
        </div>
        <!-- nav-fixed -->
    </header>