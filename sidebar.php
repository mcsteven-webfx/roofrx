<aside class="page-sidebar">
    <?php
        dynamic_sidebar( 'sidebar' );
        $home_url   = get_home_url();
    ?>
    <div class="widget widget_block widget_search" id="block-2">
        <form class="wp-block-search__button-outside wp-block-search__text-button wp-block-search" role="search" action="<?php echo esc_url( $home_url ); ?>" method="get">
            <label for="wp-block-search__input-1" class="wp-block-search__label">Search</label>
            <div class="wp-block-search__inside-wrapper">
                <input type="search" id="wp-block-search__input-1" class="wp-block-search__input" name="s" value="" placeholder="Search our site...">
                <input type="hidden" name="post_type" value="post" />
                <button type="submit" class="wp-block-search__button ">Search</button>
            </div>
        </form>
    </div>
</aside>
