<?php get_header(); ?>

<?php
    get_template_part('partials/masthead');
    $home_url   = get_home_url();
?>

<section class="page-404">
    <div class="page-404-wrapper">
        <div class="container">
            <!-- Use https://blendedwaxes.com/404 as an example -->

            <!-- Add image buttons below for top ~4 pages - one should be the homepage, PM to specify the others in specs -->
            <section class="imgbtns-404">
                <div class="row">
                    <div class="col-xxs-12">
                        <h2>Explore one of these pages instead:</h2>
                    </div>
                    <div class="col-sm-4">
                        <!-- image button -->
                        <a href="<?php echo get_permalink(31); ?>">
                            <div class="buttons-column">
                                <div class="buttons-image">
                                    <?php
                                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                        echo fx_get_image_tag( 522, 'img-responsive', $skip_lazy );
                                        $skip_lazy = false;
                                    ?>
                                </div>
                                <div class="buttons-info">
                                    <div class="buttons-info-text">
                                        <h4><?php echo get_the_title(31); ?></h4>
                                        <span class="btn btn-secondary">Learn more about  <?php echo get_the_title(31); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <!-- image button -->
                        <a href="<?php echo get_permalink(58); ?>">
                            <div class="buttons-column">
                                <div class="buttons-image">
                                    <?php
                                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                        echo fx_get_image_tag( 297, 'img-responsive', $skip_lazy );
                                        $skip_lazy = false;
                                    ?>
                                </div>
                                <div class="buttons-info">
                                    <div class="buttons-info-text">
                                        <h4><?php echo get_the_title(58); ?></h4>
                                        <span class="btn btn-secondary">Learn more about  <?php echo get_the_title(58); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <!-- image button -->
                        <a href="<?php echo get_permalink(77); ?>">
                            <div class="buttons-column">
                                <div class="buttons-image">
                                    <?php
                                        $skip_lazy = true; // skip lazy loading for first image to improve paint times
                                        echo fx_get_image_tag( 305, 'img-responsive', $skip_lazy );
                                        $skip_lazy = false;
                                    ?>
                                </div>
                                <div class="buttons-info">
                                    <div class="buttons-info-text">
                                        <h4><?php echo get_the_title(77); ?></h4>
                                        <span class="btn btn-secondary">Learn more about  <?php echo get_the_title(77); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </section>

            <section class="links-404">
                <div class="row">
                    <div class="col-xxs-12 col-md-6">
                        <div class="search-404">
                            <h4>Or, try searching our site:</h4>
                            <div class="search-content">
                                <form role="search" action="<?php echo esc_url( $home_url ); ?>" method="get">
                                    <input type="text" name="s" id="s" value="" placeholder="Search">
                                    <button type="submit"><i class="icon-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxs-12 col-md-6">
                        <div class="contact-404">
                            <h4>Still can't find what you're looking for?</h4>
                            <a href="<?php echo esc_url( $home_url ); ?>/contact-us" class="btn">Contact Us Today!</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="icon-shape-contact hidden-md-down">
        <?php
            $skip_lazy = true; // skip lazy loading for first image to improve paint times
            echo fx_get_image_tag( 870, 'img-responsive', $skip_lazy );
            $skip_lazy = false;
        ?>
    </div>
</section>

<?php get_footer(); 