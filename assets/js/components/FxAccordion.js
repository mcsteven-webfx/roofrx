// create FxAccordion object for all instances on page
( () => {
	document.addEventListener( 'DOMContentLoaded', () => {
		document.querySelectorAll('.js-accordion').forEach( el => {
			new FxAccordion( el )
		})
	})
}) ()




class FxAccordion {

	constructor( el ) {
		this.el 		= el

		this.toggles 	= []
		this.blocks 	= []
		this.activeId 	= null

		this.init()
	}


	init() {
		const self = this

		self.blocks = self.el.querySelectorAll('.js-accordion-item')
		for( const block of self.blocks ) {
			const headline 	= block.querySelector('.js-accordion-headline')

			if( null !== headline ) {
				this.toggles.push( headline )
			}
		}

		self.findActivePanel();

		self.bind();
	}


	bind() {
		const self = this

		for( const block of self.blocks ) {
			block.addEventListener( 'click', self.handleToggleClick.bind( self ) )
		}
	}

	findActivePanel() {

        // first, check if there's a currently active block
        const activeBlock = Array.from( this.blocks ).find( block => block.classList.contains('is-expanded') )

        if( undefined !== activeBlock ) {
            this.activeId = activeBlock.dataset.accordionId
        }

		// if no currently active panels, let's activate the first panel
        if( null === this.activeId ) {
            const first = this.blocks.item( 0 )

            if( null !== first ) {
                this.activeId = first.dataset.accordionId
            }
        }

        this.updateBlockStates()

	}


	handleToggleClick( e ) {
		const toggle 	= e.target,
			parent 		= toggle.closest('.js-accordion-item'),
			blockId 	= parseInt( parent.dataset.accordionId )

		this.setActiveId( blockId )
	}


	updateBlockStates() {
		const self = this

		for( const block of self.blocks ) {
			const blockId 	= parseInt( block.dataset.accordionId ),
				blockBtn 	= block.querySelector('.js-accordion-button'),
				isExpanded 	= blockId === parseInt( this.activeId )

			block.classList.toggle( 'is-expanded', isExpanded )
			block.classList.toggle( 'icon--collapse', isExpanded )

			if( null !== blockBtn ) {
				blockBtn.classList.toggle( 'icon--expand', !isExpanded )
			}
		}

		self.emitStateChange()
	}


	setActiveId( activeId = '' ) {

		// if invalid ID (or ID is already active), collapse all
		if( this.activeId === null ) {
			activeId = null
		}

		this.activeId = activeId

		this.updateBlockStates()
	}


	emitStateChange() {
		const event = new CustomEvent(
			'fxAccordionStateChange',
			{
				detail: {
					activeId: this.activeId
				}
			}
		)

		this.el.dispatchEvent( event )
	}

}
