var FX = ( function( FX, $ ) {

	$( () => {
		FX.General.init();
	})

    $( () => {
        FX.ExternalLinks.init();
	})

    $( () => {
        FX.Menu.init();
	})

    $(function() {
		FX.MobileMenu.init();
    });


	$(window).on( 'load', () => {
		FX.BackToTop.init()
	})

	$( () => {
        FX.Affix.init();
	})

	$( () => {
		FX.ExitIntent.init();
	})

	/**
	 * Display a exit intent popup
	 * @type {Object}
	 */
	FX.ExitIntent = {
		init() {
			this.bind();
		},

		bind() {
			$(document).mouseleave(function() {
				if( $('.page-popup').hasClass('hidden') ){

				} else {
					$('.page-popup').fadeIn();
				}


			});

			$('.page-popup .icon-close').click(function(){
				$('.page-popup').fadeOut();
				$('.page-popup').addClass('hidden');
			})
		}
	};

	/**
	 * Display scroll-to-top after a certain amount of pixels
	 * @type {Object}
	 */
	FX.BackToTop = {
		$btn: null,

		init() {
			this.$btn = $('.back-to-top');

			if( this.$btn.length ) {
				this.bind();
			}
		},

		bind() {
			$(window).on( 'scroll load', this.maybeShowButton.bind( this ) );
			this.$btn.on( 'click', this.scrollToTop );
		},

		maybeShowButton() {
			if( $( window ).scrollTop() > 100 ) {
				this.$btn.removeClass( 'hide' );
			} else {
				this.$btn.addClass( 'hide' );
			}
		},

		scrollToTop() {
			$(window).scrollTop( 0 );
		}
	};

	FX.General = {
		init() {
			this.bind();
		},

		bind() {

			$('a[href*=".pdf"]').each( e => {
				$(this).attr('target', '_blank');
			});

			$('body').fitVids();

			$('input,textarea').focus( () => {
				$(this).removeAttr('placeholder');
			});

			$(".search-icon").click(function(){
				$(".search-div ").slideToggle();
				$(".search-icon").toggleClass("search-cross-icon");
			})
		}
	};



	/**
	 * Mobile menu script for opening/closing menu and sub menus
	 * @type {Object}
	 */
	FX.MobileMenu = {
		init() {
			$('.nav-primary li.menu-item-has-children > a').after('<span class="sub-menu-toggle icon-Down-arrow"></span>');

			$('.sub-menu-toggle').click( function() {
				var $this = $(this),
					$parent = $this.closest("li"),
					$wrap = $parent.find("> .sub-menu");
				$wrap.toggleClass("js-toggled");
				$this.toggleClass('js-clicked');
				$this.toggleClass("js-toggled");
			});

			$('.toggle-menu').click( function() {
                $('.page-header .nav-primary').fadeToggle();
				$(this).toggleClass('cross');
			});
		}
	};



	/**
	 * Ubermenu mobile menu toggle hack
	 * @type {Object}
	 */
	FX.Menu = {
		windowWidth: 		0,
		$ubermenu: 			$('#ubermenu-nav-main-33'),
		$topLevelMenuItems: null,
		$mobileMenuToggle: 	$('.ubermenu-responsive-toggle'),


        init() {
            this.setMenuClasses();
			this.setSubMenuClasses();

			this.$topLevelMenuItems = this.$ubermenu.children('.ubermenu-item-level-0');
			this.bind();
        },

        setMenuClasses() {
            let windowWidth = $( window ).innerWidth();

            if ( windowWidth == this.windowWidth ) {
                return;
            }

            this.windowWidth = windowWidth;

            if ( this.windowWidth < 1025 ) {
                $('.ubermenu-item-has-children').each( () => {
                    $(this).removeClass('ubermenu-has-submenu-drop');
                });

            } else {
                $('.ubermenu-item-has-children').each( () => {
                    $(this).addClass('ubermenu-has-submenu-drop');
                });
            }
        },

		setSubMenuClasses() {
			$('.ubermenu-item-has-children').each( () => {
                $(this).children('a').each( () => {
                    let $this = $(this);
                    $this.children('.ubermenu-sub-indicator').clone().insertAfter( $this).addClass('submenu-toggle hidden-md-up');
                    $this.children('.ubermenu-sub-indicator').addClass('hidden-sm-down');
                });
			});
		},

        bind() {
			$(window).on( 'resize', this.setMenuClasses.bind(this) );

			$('.submenu-toggle').on( 'touchstart click', this.toggleNextLevel );

			this.$topLevelMenuItems.on( 'ubermenuopen', this.handleUbermenuOpen.bind( this ) )
			this.$topLevelMenuItems.on( 'ubermenuclose', this.handleUbermenuClose.bind( this ) )

			this.$mobileMenuToggle.on( 'ubermenutoggledopen', this.handleUbermenuOpen.bind( this ) )
			this.$mobileMenuToggle.on( 'ubermenutoggledclose', this.handleUbermenuClose.bind( this ) )
		},

		handleUbermenuOpen( e ) {
			const self = this,
				$container = self.$ubermenu.closest('.desktop-menu')

			$(document.body).addClass('menu-is-active')

			$container.addClass('menu-is-active')
			self.$mobileMenuToggle.addClass('menu-is-active')
		},


		handleUbermenuClose( e ) {
			const self = this,
				$container = self.$ubermenu.closest('.desktop-menu')

			$(document.body).removeClass('menu-is-active')
			$container.removeClass('menu-is-active')
			self.$mobileMenuToggle.removeClass('menu-is-active')
		},

        toggleNextLevel( e ) {
            let $this = $( this );

			e.preventDefault();

            $this.toggleClass('fa-angle-down').toggleClass('fa-times');
            $this.parent().toggleClass('ubermenu-active');
            if( $this.parent().hasClass('ubermenu-active') ) {
                $this.parent().siblings('.ubermenu-active').removeClass('ubermenu-active').children('.submenu-toggle').addClass('fa-angle-down').removeClass('fa-times');
            }
        }
	}

	/**
	 * Force External Links to open in new window.
	 * @type {Object}
	 */
	FX.ExternalLinks = {
		init() {
			var siteUrlBase = FX.siteurl.replace( /^https?:\/\/((w){3})?/, '' );

			$( 'a[href*="//"]:not([href*="'+siteUrlBase+'"])' )
				.not( '.ignore-external' )
				.addClass( 'external' )
				.attr( 'target', '_blank' )
				.attr( 'rel', 'noopener' );
		}
	};

	/**
	 * Affix
	 * Fixes sticky items on scroll
	 * @type {Object}
	 */
 	FX.Affix = {
		init: function() {
            this.windowHeight = $(window).height();
            this.bind();
        },

        bind: function(e) {
            $(window).on('scroll', this.scroll);
            $(window).on('resize', this.updateWindowHeight);
        },

		scroll:function(){
			var fromTopPx = 200;
			var scrolledFromtop = $(window).scrollTop();
			if(scrolledFromtop > 150){ $('.page-header').addClass('hideheader'); }
			else{ $('.page-header').removeClass('hideheader'); }
			if(scrolledFromtop > fromTopPx){ $('.page-header').addClass('js-scrolled'); $('.mastheads').addClass('scrolled'); $('.masthead-inner').addClass('scrolled'); }
			else{ $('.page-header').removeClass('js-scrolled'); $('.masthead').removeClass('scrolled'); $('.masthead-inner').removeClass('scrolled'); } },
	};

	/**
	 * FX.SmoothAnchors
	 * Smoothly Scroll to Anchor ID
	 * @type {Object}
	 */
	FX.SmoothAnchors = {
		hash: null,

		init() {
			this.hash = window.location.hash;

			if( '' !== this.hash ) {
				this.scrollToSmooth( this.hash );
			}

			this.bind();
		},

		bind() {
			$( 'a[href^="#"]' ).on( 'click', $.proxy( this.onClick, this ) );
		},

		onClick( e ) {
			e.preventDefault();

			var target = $( e.currentTarget ).attr( 'href' );

			this.scrollToSmooth( target );
		},

		scrollToSmooth( target ) {
			var $target = $( target ),
				headerHeight = 0

			$target = ( $target.length ) ? $target : $( this.hash );

			if ( $target.length ) {
				var targetOffset = $target.offset().top - headerHeight;

				$( 'html, body' ).animate({
					scrollTop: targetOffset
				}, 600 );

				return false;
			}
		}
	};



	return FX;

} ( FX || {}, jQuery ) );
