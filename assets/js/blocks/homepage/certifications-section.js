var FX = ( function( FX, $ ) {

    $( () => {
        FX.Certifications.init()
    })

    FX.Certifications = {
        $slider: null,

        init() {

            var $slider = $('.certifications-slider');
            var $progressBar = $('.certifications .progress');
            var $progressBarLabel = $( '.slider__label' );

            $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                var calc = ( (nextSlide) / (slick.slideCount-1) ) * 100;

                $progressBar
                .css('background-size', calc + '% 100%')
                .attr('aria-valuenow', calc );

                $progressBarLabel.text( calc + '% completed' );
            });

            $slider.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 800,
                dots: false,
                arrows: false,
                swipeToSlide: true,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: "unslick",
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                        }
                    }
                  ]
            });
        },

        applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
        }
    }

    return FX

} ( FX || {}, jQuery ) )