var FX = ( function( FX, $ ) {

    $( () => {
        FX.Testimonials.init()
    })

    FX.Testimonials = {
        $slider: null,

        init() {

            $('.testimonials-slider').slick({
                infinite: true,
                speed: 800,
                dots: false,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            });
        },

        applySlick() {
            this.$slider.slick( {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
        }
    }

    return FX

} ( FX || {}, jQuery ) )