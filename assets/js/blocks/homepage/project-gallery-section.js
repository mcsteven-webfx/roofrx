( $ => {


    window.addEventListener( 'load', () => {
        document.querySelectorAll('.js-project-gallery').forEach( el => {
            new FxProjectScroll( el )
        })
    })

    
    
    class FxProjectScroll {
    
        constructor( el ) {
            this.el = el

            this.elSlickSlider 	= this.el.querySelector('.js-project-gallery-slider')
            this.elUiSlider 	= this.el.querySelector('.js-project-gallery-ui-slider')
            this.elImages 		= this.elSlickSlider.querySelectorAll('.gallery-image')
			this.btnPrev 		= this.el.querySelector('.js-project-gallery-before')
			this.btnNext 		= this.el.querySelector('.js-project-gallery-after')

            this.$slickSlider 	= null
            this.uiSlider 		= null

			// states
			this.imageCount 	= this.elImages.length
			this.isInteracting 	= false
			this.currentSlide 	= false

            if( this.elSlickSlider && this.elUiSlider ) {
                this.applySlick()
                this.applyFancybox()
                this.createUiSlider()

				this.bind()
            }

			if( this.btnPrev && this.btnNext ) {
				this.bindBtns()
			}
        }


		bind() {
			const self = this

			// Slick slider
			$(self.$slickSlider.get( 0 ) ).on( 'afterChange', self.handleSlickInteraction.bind( self ) )

			// UI slider
			self.uiSlider.on( 'set', self.handleUiSliderInteraction.bind( self ) )
		}


		bindBtns() {
			const self = this

			self.btnPrev.addEventListener( 'click', () => {
				self.handleBtnClick( -1 )
			})

			self.btnNext.addEventListener( 'click', () => {
				self.handleBtnClick( 1 )
			})
		}


		handleBtnClick( value ) {
			const self = this

			if( self.isInteracting ) {
				return
			}
			self.isInteracting = true
			
			let currentSlide = self.currentSlide + value

			if( 0 > currentSlide ) {
				currentSlide = self.imageCount
			} else if( currentSlide > self.imageCount ) {
				currentSlide = 0
			}
			
			self.updateUiSlider( currentSlide )
			self.updateSlickSlider( currentSlide )

			setTimeout( () => {
				self.isInteracting = false
			}, 500 )
		}


		handleSlickInteraction( e, slick, currentSlide ) {
			const self = this

			if( self.isInteracting ) {
				return
			}
			self.isInteracting = true

			self.updateUiSlider( currentSlide )

			setTimeout( () =>{
				self.isInteracting = false
			}, 500 )
		}


        /**
         * Handle what happens when user interacts with slider
         * 
         * @param   {array}     values      Current slide values
         * @param   {number}    handle      Handle that caused event
         * @param   {array}     raw         Raw slider values
         * @param   {bool}      tap         True, if event caused by user tapping slider
         * @param   {array}     positions   Offset of handles
         * 
         * @return  void
         */		
		handleUiSliderInteraction( values, handle, raw, tap, positions ) {
			const self = this

			if( self.isInteracting ) {
				return
			}
			self.isInteracting = true

			self.updateSlickSlider( raw[0] )

			setTimeout( () => {
				self.isInteracting = false
			}, 500 )
		}


        applySlick() {
            this.$slickSlider = $(this.elSlickSlider).slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
                swipeToSlide: true,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                    {
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 520,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 376,
                        settings: {
                            slidesToShow: 1,
                            centerMode: true,
                        }
                    }

                  ]
            })
        }


        applyFancybox() {
            $(this.elImages).fancybox({
                arrows: true,
                modal: true,
                smallBtn: true,
                toolbar: true,
            })
        }


        createUiSlider() {
			const self = this

            self.uiSlider = noUiSlider.create( self.elUiSlider, {
                start: 0,
                step: 1,
                connect: 'lower',
                range: {
                    min: 0,
                    max: self.imageCount
                }
            })
        }


		updateUiSlider( value ) {
			const self = this

			if( !self.uiSlider ) {
				return
			}

			self.uiSlider.set( value )
			self.currentSlide = value
        }


		updateSlickSlider( value ) {
			const self = this

			if( !self.$slickSlider ) {
				return
			}		
			
			self.$slickSlider.slick( 'slickGoTo', value, false )
			self.currentSlide = value
		}
    
    
    }



}) ( jQuery )
