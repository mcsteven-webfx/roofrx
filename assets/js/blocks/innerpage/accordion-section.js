var FX = ( function( FX, $ ) {

    $( () => {
        FX.TabFilter.init()
    })

    FX.TabFilter = {
        $slider: null,

        init() {
            $(".accordion h3").eq(0).addClass("active");
            $(".accordion-content").eq(0).show();

            $(".accordion h3").click(function(){
                $(this).next(".accordion-content").slideToggle("slow")
                .siblings(".accordion-content:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings("h3").removeClass("active");
            });
        },
    }

    return FX

} ( FX || {}, jQuery ) )
