var FX = ( function( FX, $ ) {

    $( () => {
        FX.loadmore.init();
    })

    FX.loadmore = {

        init() {
            this.$testimonialContainer = $('.testimonial-listings')

            if( this.$testimonialContainer.length ) {
                this.testimonialLoadMore();
            }
        },

        testimonialLoadMore() {
            var post_shown = $('.post-shown').text(),
                total_post = $('.total-post-count').text(),
                percentage = ((post_shown / total_post) * 100).toFixed(2);

            $('.loading-bar-active').css('width', percentage+'%');

            if (post_shown == total_post) {
                $('.js-testimonial-loadmore').addClass('disabled');
            }

            $('.js-testimonial-loadmore').click(function(e) {
                e.preventDefault();

                var post_shown = $('.post-shown').text(),
                    total_post = $('.total-post-count').text(),
                    percentage = ((post_shown / total_post) * 100).toFixed(2);

                var data = {
                    'action': 'loadmore',
                    'query': testimonial_loadmore_params.posts
                };

                $.ajax({
                    url : testimonial_loadmore_params.ajaxurl,
                    data: data,
                    type: 'POST',
                    success:function(data){
                        if( data ) {
                            $('.testimonial-listings ul').html(data);
                            $('.post-shown').text(total_post);
                            $('.loading-bar-active').css('width', '100%');
                        }
                    },
                    complete: function() {
                        $('.js-testimonial-loadmore').addClass('disabled');
                    }
                });
            });
        }
    }

    return FX

} ( FX || {}, jQuery ) )