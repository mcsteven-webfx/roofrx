<?php

/**
 * Register FX blocks
 *
 * fx_register_block() is, at its core, a wrapper function for acf_register_block_type with additional parameters for
 * our supporting functionality
 *
 * @see Guru card: https://app.getguru.com/card/Tn9zzk8c/FX-ACF-Blocks
 * @see more info for acf_register_block_type(): https://www.advancedcustomfields.com/resources/acf_register_block_type/
 *
 * Below is a reference for the parameters you can pass to fx_register_block(). You can also pass any setting from
 * acf_register_block_type() to fx_register_acf_block().
 *
 * Required arguments: "name", "title", and "template"
 *
 */


/**
 * General blocks
 *
 * These blocks are intended to be used anywhere, including the homepage and innerpage.
 *
 * Block template path: /themes/fx/block-templates/general
 * Stylesheet path:     /themes/fx/assets/css/general
 * Script path:         /themes/fx/assets/js/general
 *
 */


/**
 * Create a "FX General Blocks" category in the block editor. Use "fx-general-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX General Blocks', 'fx-general-blocks' );


/**
 * Plan WYSIWYG block for general usage
 *
 */
fx_register_block(
    [
        'name'          => 'wysiwyg',
        'title'         => 'WYSIWYG',
        'template'      => 'general/wysiwyg.php',
        'description'   => 'A basic "What you see is what you get" editor.',
        'css'           => 'general/wysiwyg.css',
        'post_types'    => [],
    ]
);


/**
 * To avoid issues with CF7 assets, we're creating our own CF7 block. You shouldn't need to touch this section.
 *
 */
$cf7_settings = [
    'name'          => 'cf7-block',
    'title'         => 'CF7 Block',
    'template'      => 'general/cf7-block.php',
    'description'   => 'Adds a CF7 block to page',
    'css_deps'      => [ 'fx_choices_custom', 'contact-form-7' ],
    'js_deps'       => [ 'contact-form-7', 'wpcf7-recaptcha', 'google-recaptcha' ],
    'keywords'      => [ 'cf7', 'contact', 'form' ],
    'mode'          => 'edit',
    'post_types'    => [], // all post types
];
$cf7_icon = WP_PLUGIN_DIR . '/contact-form-7/assets/icon.svg';
if( file_exists( $cf7_icon ) ) {
    $cf7_settings['icon'] = file_get_contents( $cf7_icon );
}
fx_register_block( $cf7_settings );

fx_register_block(
    [
        'name'          => 'general-cta-bar',
        'title'         => 'CTA Bar',
        'template'      => 'general/cta-bar.php',
        'description'   => 'CTA Bar block for pages.',
        'css'           => 'general/cta-bar.css',
        'category'      => 'fx-general-blocks',
        'post_types'    => [], // all post types
    ]
);


/**
 * Homepage blocks
 *
 * These blocks are intended to be used ONLY on the homepage.
 *
 * Block template path: /themes/fx/block-templates/homepage
 * Stylesheet path:     /themes/fx/assets/css/homepage
 * Script path:         /themes/fx/assets/js/homepage
 *
 */

/**
 * Create a "FX Homepage Blocks" category in the block editor. Use "fx-homepage-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX Homepage Blocks', 'fx-homepage-blocks' );


/**
 * This is the main homepage "outer block." All other homepage blocks should be added within this block in the Block
 * Editor and in block-templates/homepage/homepage-block.php
 *
 */
fx_register_block(
    [
        'name'          => 'homepage-block',
        'title'         => 'Homepage',
        'template'      => 'homepage/homepage-block.php',
        'description'   => 'The main content block for the homepage',
        'mode'          => 'preview',
        'supports'      => [ 'jsx' => true ], // enables support for inner blocks
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-masthead-banner',
        'title'         => 'Homepage - Masthead Banner',
        'template'      => 'homepage/masthead-banner.php',
        'description'   => 'Banner block for the homepage masthead.',
        'css'           => 'homepage/masthead-banner.css',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-roofing-services',
        'title'         => 'Homepage - Roofing Services Section',
        'template'      => 'homepage/roofing-services.php',
        'description'   => 'Roofing Services Section block for the homepage.',
        'css'           => 'homepage/roofing-services.css',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-about-section',
        'title'         => 'Homepage - About Section',
        'template'      => 'homepage/about-section.php',
        'description'   => 'About Section block for the homepage.',
        'css'           => 'homepage/about-section.css',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-service-area-section',
        'title'         => 'Homepage - Service Area Section',
        'template'      => 'homepage/service-area-section.php',
        'description'   => 'Service Area Section block for the homepage.',
        'css'           => 'homepage/service-area-section.css',
        'js'            => 'homepage/service-area-slider.js',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-project-gallery-section',
        'title'         => 'Homepage - Project Gallery Section',
        'template'      => 'homepage/project-gallery-section.php',
        'description'   => 'Project Gallery Section block for the homepage.',
        'css'           => 'homepage/project-gallery-section.css',
        'css_deps'      => [ 'fx_slick', 'fx_fancybox', 'fx_nouislider' ],
        'js'            => 'homepage/project-gallery-section.js',
        'js_deps'       => [ 'fx_slick', 'fx_fancybox', 'fx_nouislider' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-testimonial-section',
        'title'         => 'Homepage - Testimonial Section',
        'template'      => 'homepage/testimonial-section.php',
        'description'   => 'Testimonial Section block for the homepage.',
        'css'           => 'homepage/testimonial-section.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/testimonial-section.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-certifications-section',
        'title'         => 'Homepage - Certifications Section',
        'template'      => 'homepage/certifications-section.php',
        'description'   => 'Certifications Section block for the homepage.',
        'css'           => 'homepage/certifications-section.css',
        'js'            => 'homepage/certifications-section.js',
        'category'      => 'fx-homepage-blocks',
    ]
);

/**
 * Innerpage blocks
 *
 * These blocks are intended to be used ONLY on innerpages
 *
 * Block template path: /themes/fx/block-templates/innerpage
 * Stylesheet path:     /themes/fx/assets/css/innerpage
 * Script path:         /themes/fx/assets/js/innerpage
 *
 */

/**
 * Create a "FX Innerpage Blocks" category in the block editor. Use "fx-innerpage-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX Innerpage Blocks', 'fx-innerpage-blocks' );

fx_register_block(
    [
        'name'          => 'innerpage-testimonials',
        'title'         => 'Innerpage - Testimonials',
        'template'      => 'innerpage/testimonials.php',
        'description'   => 'Testimonials block for the innerpage.',
        'css'           => 'innerpage/testimonials.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-full-width-image-background',
        'title'         => 'Innerpage - Full-Width Image Background Section',
        'template'      => 'innerpage/full-width-image-background-section.php',
        'description'   => 'Full-Width Image Background Section block for the innerpage.',
        'css'           => 'innerpage/full-width-image-background-section.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-image-buttons-section',
        'title'         => 'Innerpage - Image Buttons Section',
        'template'      => 'innerpage/image-buttons-section.php',
        'description'   => 'Image Buttons Section block for the innerpage.',
        'css'           => 'innerpage/image-buttons-section.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-image-text-section',
        'title'         => 'Innerpage - Image Text Section',
        'template'      => 'innerpage/image-text-section.php',
        'description'   => 'Image Text Section block for the innerpage.',
        'css'           => 'innerpage/about-section.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-contact-text-section',
        'title'         => 'Innerpage - Contact Text Section',
        'template'      => 'innerpage/contact-text-section.php',
        'description'   => 'Contact Text Section block for the innerpage.',
        'css'           => 'innerpage/contact-text-section.css',
        'category'      => 'fx-innerpage-blocks',
        'post_types'    => ['page', 'service-areas'],
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-accordion-section',
        'title'         => 'Innerpage - Accordion Section',
        'template'      => 'innerpage/accordion-section.php',
        'description'   => 'Accordion Section block for the innerpage.',
        'css'           => 'innerpage/accordion-section.css',
        'js'            => 'innerpage/accordion-section.js',
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-project-gallery-section',
        'title'         => 'Innerpage - Project Gallery Section',
        'template'      => 'homepage/project-gallery-section.php',
        'description'   => 'Project Gallery Section block for the innerpage.',
        'css'           => 'innerpage/project-gallery-section.css',
        'css_deps'      => [ 'fx_slick', 'fx_fancybox'],
        'js'            => 'homepage/project-gallery-section.js',
        'js_deps'       => [ 'fx_slick', 'fx_fancybox' ],
        'category'      => 'fx-innerpage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'innerpage-three-columns-image-buttons-section',
        'title'         => 'Innerpage - Three Columns Image Buttons Section',
        'template'      => 'innerpage/three-columns-image-buttons-section.php',
        'description'   => 'Three Columns Image Buttons Section block for the innerpage.',
        'css'           => 'innerpage/three-columns-image-buttons-section.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);
