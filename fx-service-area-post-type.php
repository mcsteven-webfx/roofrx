<?php
/**
 * Plugin Name: FX Custom Post Type
 * Plugin URI: https://www.webfx.com
 * Description: Custom Post Types
 * Version: 1.1
 * Author: The WebFX Team
 * Author URI: https://www.webfx.com
 **/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


class FX_CUSTOM_POST_TYPE {
    protected static $instance;

    public static function instance() {
        if ( ! isset( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function __construct() {
        add_action( 'init', array( $this, 'register_post_type' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
    }


    /**
     * Registers custom post type
     *
     * @return void
     */

    public function register_post_type() {
        $post_types = array(
            //Service Area
            array(
                'the_type'	=>	'service-areas',
                'single'	=>	'Service Area',
                'plural'	=>	'Service Areas',
                'menu_icon' =>  'dashicons-admin-site'
            )
        );

        foreach($post_types as $type) {

            $the_type   =   $type['the_type'];
            $single     =   $type['single'];
            $plural     =   $type['plural'];
            $menu_icon  =   $type['menu_icon'];

            if(!empty($type['slug'])) {
                $slug = $type['slug'];
            } else {
                $slug = $type['the_type'];
            }

            // for supports
            $supports = empty($type['supports']) ? array('title','editor','thumbnail','page-attributes','excerpt') : $type['supports'];

            $labels = array(
                'name'                  => _x($plural, 'post type general name'),
                'singular_name'         => _x($single, 'post type singular name'),
                'add_new' 				=> _x('New '.$single, $single),
                'add_new_item' 			=> __('Add '.$single),
                'new_item' 				=> __('New ' .$single),
                'edit_item' 			=> __('Edit ' . $single),
                'view_item' 			=> __('View ' . $single),
                'search_items' 			=> __('Search ' . $plural),
                'not_found' 			=> __('No ' . $plural . ' found'),
                'not_found_in_trash'	=> __('No ' . $plural . ' found in Trash')
              );

              $args = array(
                'labels'              => $labels,
                'public'              => true,
                'publicly_queryable'  => true,
                'show_ui'             => true,
                'show_in_nav_menus'   => true,
                'show_in_menu'        => true,
                'show_in_admin_bar'   => true,
                'menu_position'       => 5,
                'menu_icon'           => $menu_icon,
                'has_archive'         => true,
                'show_in_rest'        => true,
                'supports'            => $supports,
                'rewrite'             => array( 'slug' => $the_type )
              );

            register_post_type($the_type, $args);

        } // end of foreach
    }

    public function register_taxonomies()  {

        $a_taxonomies = array(
            // Service Areas Category
            array(
                "the_type"   		=> "service-areas",
                "the_taxonomies"	=> "service-areas_category"
            )
        );

        foreach ($a_taxonomies as $a_taxonomy) {

            $s_the_type = $a_taxonomy["the_type"];
            $s_the_taxonomies = $a_taxonomy["the_taxonomies"];

            $a_labels = array(
                'name'              => 'Categories',
                'singular_name'     => 'Category',
                'search_items'      => 'Search Categories',
                'all_items'         => 'All Categories',
                'parent_item'       => 'Parent Category',
                'parent_item_colon' => 'Parent Category:',
                'edit_item'         => 'Edit Category',
                'update_item'       => 'Update Category',
                'add_new_item'      => 'Add New Category',
                'new_item_name'     => 'New Category Name',
                'menu_name'         => 'Categories'
            );

            $a_args = array(
                'labels'            => $a_labels,
                'rewrite'           => array(
                    'with_front' => false,
                ),
                'hierarchical'      => true,
                'show_admin_column' => true
            );

            register_taxonomy( $s_the_taxonomies, $s_the_type, $a_args );
        }
    }
}

FX_CUSTOM_POST_TYPE::instance();
